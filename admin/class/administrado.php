<?php
    class Administrador
    {   
        // atributos privados
        private $id;
        private $nome;
        private $email;
        private $login;
        private $senha;

        //DECLARANDO METODOS DE ACESSO (getters)\\
        public function getId()
        {
            return $this->id;
        }
        public function setId($value)
        {
            $this->id = $value;
        }

        //FUNCTION -------------------- NAME\\
        public function getNome()
        {
            return $this->nome;
        }
        public function setNome($value)
        {
            $this->nome = $value;
        }

        //FUNCTION --------------- --- - EMAIL\\
        public function geEmail()
        {
            return $this->email;
        }
        public function setEmail($value)
        {
            $this->email = $value;
        }

        //FUNCTION --------------------- LOGIN\\
        public function getLogin()
        {
            return $this->login;
        }
        public function setLogin($value)
        {
            $this->login = $value;
        }

        //FUNCTION ---------------------- SENHA\\
        public function getSenha()
        {
            return $this->senha;
        }
        public function setSenha($value)
        {
            $this->senha = $value;
        }

        // Mostra o administrador do banco pelo id
        public function loadById($_id)
        {
            $sql = new Sql();  // uma instancia do sql \\
            $results = $sql->select("SELECT * FROM administrador WHERE id = :id",array(':id'=>$_id);
            if (count($results)>0)
            {
                $this->setData($results[0]);
            }
        }
        //FUNCTION ---------------------- Lista\\
        public static function getList()
        {
            $sql = new Sql();
            return $sql->select("SELECT * FROM administrador order by nome");
        }
        // prora nome mais rapido \\
        public static function search($nome_adm)
        {
            $sql = new Sql();
            return $sql->select("SELECT * FROM administrador WHERE nome LIKE :nome",array(":nome"=>"%".$nome_adm."%")); // .ponto. concatenar -- nome like facilita na procura por nome 

        }
        //          -------------------------------        login\\
        public function efetuarLogin($_login, $_senha)
        {
            $sql = new Sql();
            $senha_cript = md5($_senha);
            
            $results = $sql->select("SELECT * FROM administrador WHERE login = :login AND senha= :senha",array(':login'=>$_login,":senha"=>$senha_cript));
            if (count($results)>0)
            {
                $this->setData($results[0]);
            }
        }
        // busca dado
        public function setData($data)
        {
            $this->setId($data['id']);
            $this->setNome($data['nome']);
            $this->setEmail($data['email']);
            $this->setLogin($data['login']);
            $this->setNome($data['senha']);
            
        }

        // cadastrando usuario 
        public function insert()
        {
            $sql = new Sql();
            $results = $sql->select("CALL sp_adm_insert(:nome, :email,:login, :senha"),array
            (   ":nome"=>$this->getNome(),
                ":email"=>$this->getEmail(),
                ":login"=>$this->getLogin(),
                ":senha"=>md5($this->getSenha())
            ));
        if (count($results)>0)
        {
            $this->setData($results[0]);
        }
        }

        // alterando usuatio
        public function update($_id, $_nome, $_email, $_senha)
        {
            $sql = new Sql();
            $sql->query("UPDATE administrador SET nome = :nome, emal= :email, senha = :senha WHERE id = :id"),array
            ( 
                ":id"=>$_id,
                ":nome"=>$_nome,
                ":email"=>$_email,
                ":senha"=>md5($_senha)
            ));
        }
        
        // deletando categoria
        public function delete()
        {
            $sql = new Sql();
            $sql->query("DELETE FROM administrador WHERE id = :id",array(":id"=>$this->getId()));
        }

        // construtor da classe
        public function __construct($_nome="",$_email="",$_login="",$_senha="")
        {
            $this->nome = $_nome;
            $this->email = $_email;
            $this->login = $_login;
            $this->senha = $_senha;           
        }

    }
?>