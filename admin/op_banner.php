<?php
if(isset($_POST['txt_banner'])){
    require_once('conexao.php');
    $titulo_banner = $_POST['txt_banner'];
    $link_banner = $_POST['url_banner'];
    $img_banner = $_POST['img_banner'];
    $alt = $_POST['alt_banner'];
    $ativo = isset($_POST['check_ativo'])?'1':'0';
    $cmd = $cn->prepare("INSERT INTO banner (titulo_banner, link_banner, img_banner, alt, banner_ativo) VALUES (:titulo, :link, :img, :alt, :ativ)");
    $cmd->execute(array(
         ':titulo'=>$titulo_banner,
         ':link'=>$link_banner,
         ':img'=>$img_banner,
         ':alt'=>$alt,
         ':ativ'=>$ativo
        
    ));
    header('location:principal.php?link=8&msg=ok');
}
function listar_categoria(){
    require_once('conexao.php');
    $query = "select * from categoria";
    $cmd = $cn->prepare($query); //PDO
    $cmd->execute();
    return $cmd->fetchAll(PDO::FETCH_ASSOC);
}


?>