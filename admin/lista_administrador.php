<?php
    require_once('conexao.php');
    $cmd = $cn->prepare('select * from administrador');
    $cmd->execute();
    $adms_retornados = $cmd->fetchAll(PDO::FETCH_ASSOC);
    if(count($adms_retornados) > 0)
    {
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">    
    <title>Lista Administrador</title>
</head>
<body>
    <table width='100%' border="" cellpadding="0" cellspacing="1" bgcolor="">
        <tr bgcolor="#993300" align="center">
            <th width="15%" height="2" align="rigth"><font size="2" color="#fff">ID</font></th>
            <th width="25%" height="2" align="rigth"><font size="2" color="#fff">Nome</font></th>
            <th width="25%" height="2" align="rigth"><font size="2" color="#fff">Email</font></th>
            <th width="20%" height="2" align="rigth"><font size="2" color="#fff">Login</font></th>
            <th colspan="2" align="center"><font size="2" color="#fff">Opções</font></th>
        </tr>
        <?php
            foreach($adms_retornados as $adm)
            {
        ?>
        <tr>
            <td><?php echo $adm['id']?></td>
            <td><?php echo $adm['nome']?></td>
            <td><?php echo $adm['email']?></td>
            <td><?php echo $adm['login']?></td>
            <td align="center"><font size="2" face="verdana, arial" color="#000"></font><a href="principal.php?link=">Alterar</a></td>
            <td align="center"><font size="2" face="verdana, arial" color="#000"></font><a href="principal.php?link=">Excluir</a></td>
        </tr>
        <?php
            }
        }
        ?>
    </table>
</body>
</html>