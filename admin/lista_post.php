<?php
require_once('conexao.php');
$query = "select * from banner";
$cmd = $cn->prepare($query); //PDO
$cmd->execute(); 
$post_retornadas = $cmd->fetchAll(PDO::FETCH_ASSOC);
if(count ($post_retornadas)>0){
    // print_r($categoria_retornadas);
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>

    <title>Lista Post</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <table id="tb_post" width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="#42154">
        <tr bgcolor="#93774" allign="center">
        <th width="10%" height="2"><font size="2" color="#fff">Código</font></th>
            <th width="10%" height="2"><font size="2" color="#fff">Id Categoria</font></th>
            <th width="30%" height="2"><font size="2" color="#fff">Título</font></th>
            <th width="40%" height="2"><font size="2" color="#fff">Descrição</font></th>
            <th width="40%" height="2"><font size="2" color="#fff">Imagem</font></th>
            <th width="40%" height="2"><font size="2" color="#fff">Visitas</font></th>
            <th width="30%" height="2"><font size="2" color="#fff">Ativo</font></th>
            <th colspan="20%"><font size="2" color="#fff">Opções</font></th>
        </tr>
        <?php 
        foreach($post_retornadas as $post){
        ?>
         <tr>
            <td><font size="2" face="verdana, arial" color="#0cc"><?php echo $post['id_post']; ?></font></td>
            <td><font size="2" face="verdana, arial" color="#cc0"><?php echo $post['id_categoria']; ?></font></td>
            <td><font size="2" face="verdana, arial" color="#cc0"><?php echo $post['titulo']; ?></font></td>
            <td><font size="2" face="verdana, arial" color="#cc0"><?php echo $post['descricao']; ?></font></td>
            <td><font size="2" face="verdana, arial" color="#cc0"><?php echo $post['img']; ?></font></td>
            <td><font size="2" face="verdana, arial" color="#cc0"><?php echo $post['visita']; ?></font></td>
            <td><font size="2" face="verdana, arial" color="#cc0"><?php echo $post['data']; ?></font></td>
            <td><font size="2" face="verdana, arial" color="#c0c"><?php echo $post['ativo']=='1'?'Sim':'Não'; ?></font></td>
            <td align="center"><font size="2" face="verdana, arial" color="#fff"><a href="principal.php?link=">Alterar</a></font></td>
            <td align="center"><font size="2" face="verdana, arial" color="#fff"><a href="principal.php?link=">Excluir</a></font></td>
        </tr>
<?php }} ?>
    </table>
</body>
</html>


